<?php
App::uses('PurchaseOrder', 'Model');

/**
 * PurchaseOrder Test Case
 *
 */
class PurchaseOrderTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.purchase_order',
		'app.clinic',
		'app.sale',
		'app.customer',
		'app.invoice',
		'app.invoice_item',
		'app.product',
		'app.sale_item',
		'app.supplier',
		'app.purchase_order_item'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PurchaseOrder = ClassRegistry::init('PurchaseOrder');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PurchaseOrder);

		parent::tearDown();
	}

}
