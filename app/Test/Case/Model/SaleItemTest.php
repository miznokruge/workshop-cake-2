<?php
App::uses('SaleItem', 'Model');

/**
 * SaleItem Test Case
 *
 */
class SaleItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sale_item',
		'app.sale',
		'app.clinic',
		'app.purchase_order',
		'app.supplier',
		'app.purchase_order_item',
		'app.product',
		'app.invoice_item',
		'app.invoice',
		'app.customer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SaleItem = ClassRegistry::init('SaleItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SaleItem);

		parent::tearDown();
	}

/**
 * testUpdateTotal method
 *
 * @return void
 */
	public function testUpdateTotal() {
		$this->markTestIncomplete('testUpdateTotal not implemented.');
	}

/**
 * testUpdateStok method
 *
 * @return void
 */
	public function testUpdateStok() {
		$this->markTestIncomplete('testUpdateStok not implemented.');
	}

}
