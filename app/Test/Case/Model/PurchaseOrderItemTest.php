<?php
App::uses('PurchaseOrderItem', 'Model');

/**
 * PurchaseOrderItem Test Case
 *
 */
class PurchaseOrderItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.purchase_order_item',
		'app.product',
		'app.invoice_item',
		'app.invoice',
		'app.sale',
		'app.clinic',
		'app.purchase_order',
		'app.supplier',
		'app.customer',
		'app.sale_item'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PurchaseOrderItem = ClassRegistry::init('PurchaseOrderItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PurchaseOrderItem);

		parent::tearDown();
	}

/**
 * testUpdateTotal method
 *
 * @return void
 */
	public function testUpdateTotal() {
		$this->markTestIncomplete('testUpdateTotal not implemented.');
	}

/**
 * testUpdateStok method
 *
 * @return void
 */
	public function testUpdateStok() {
		$this->markTestIncomplete('testUpdateStok not implemented.');
	}

}
