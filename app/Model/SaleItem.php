<?php

App::uses('AppModel', 'Model');

/**
 * SaleItem Model
 *
 * @property Sale $Sale
 * @property Product $Product
 */
class SaleItem extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'sale_id' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'product_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'qty' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Sale' => array(
            'className' => 'Sale',
            'foreignKey' => 'sale_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Product' => array(
            'className' => 'Product',
            'foreignKey' => 'product_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public function afterSave($created, $options = array()) {
        parent::afterSave($created, $options);
        $this->updateTotal($this->data['SaleItem']['sale_id']);
    }
    
    public function updateTotal($id) {
        $h = $this->query("SELECT SUM(qty*price) as total FROM sale_items WHERE sale_id=" . $id . " "
                . "GROUP BY sale_id LIMIT 1");
        $total=$h[0][0]['total'];
        $this->query("UPDATE sales SET total_amount = ".$total." WHERE id =".$id);
    }
    
    public function updateStok($product_id,$qty) {
        $this->query("UPDATE products SET stok = stok-".$qty." WHERE id =".$id);
    }
}
