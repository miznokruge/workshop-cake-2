<?php

App::uses('AppModel', 'Model');

/**
 * PurchaseOrderItem Model
 *
 * @property Product $Product
 * @property PurchaseOrder $PurchaseOrder
 */
class PurchaseOrderItem extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'qty' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'product_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'purchase_order_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Product' => array(
            'className' => 'Product',
            'foreignKey' => 'product_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PurchaseOrder' => array(
            'className' => 'PurchaseOrder',
            'foreignKey' => 'purchase_order_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function afterSave($created, $options = array()) {
        parent::afterSave($created, $options);
        $this->updateTotal($this->data['PurchaseOrderItem']['purchase_order_id']);
    }

    public function updateTotal($id) {
        $h = $this->query("SELECT SUM(qty*price) as total FROM purchase_items WHERE purchase_order_id=" . $id . " "
                . "GROUP BY purchase_order_id LIMIT 1");
        $total = $h[0][0]['total'];
        $this->query("UPDATE purchase_orders SET total_amount = " . $total . " WHERE id =" . $id);
    }

    public function updateStok($product_id, $qty) {
        $this->query("UPDATE products SET stok = stok+" . $qty . " WHERE id =" . $id);
    }

}
