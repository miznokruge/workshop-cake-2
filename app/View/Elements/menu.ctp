<nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>" class="nav-link">
                                    <i class="nav-icon fa fa-th"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/sales" class="nav-link">
                                    <i class="nav-icon fa fa-money"></i>
                                    <p>
                                        Sales
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/purchase_orders" class="nav-link">
                                    <i class="nav-icon fa fa-money"></i>
                                    <p>
                                        Purchases
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/products" class="nav-link">
                                    <i class="nav-icon fa fa-circle-o text-info"></i>
                                    <p>
                                        Products
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/customers" class="nav-link">
                                    <i class="nav-icon fa fa-circle-o text-info"></i>
                                    <p>
                                        Customers
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/suppliers" class="nav-link">
                                    <i class="nav-icon fa fa-circle-o text-info"></i>
                                    <p>
                                        Suppliers
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/clinics" class="nav-link">
                                    <i class="nav-icon fa fa-circle-o text-info"></i>
                                    <p>
                                        Clinics
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/users" class="nav-link">
                                    <i class="nav-icon fa fa-circle-o text-info"></i>
                                    <p>
                                        Users
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/user_groups" class="nav-link">
                                    <i class="nav-icon fa fa-circle-o text-info"></i>
                                    <p>
                                        User Group
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/permissions" class="nav-link">
                                    <i class="nav-icon fa fa-circle-o text-info"></i>
                                    <p>
                                        permissions
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo $appConfig['base_url'] ?>/users/logout" class="nav-link">
                                    <i class="nav-icon fa fa-key text-info"></i>
                                    <p>
                                        Logout
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </nav>