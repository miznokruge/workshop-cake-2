<div class="card-body login-card-body">
    <p class="pad-btm">Sign In to your account</p>
    <?php echo $this->Form->create('User', array("class" => "m-t")); ?>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-user"></i></div>
            <input type="text" class="form-control" placeholder="Username" name="data[User][username]">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
            <input type="password" class="form-control" placeholder="Password" name="data[User][password]">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8 text-left checkbox">
            <label class="form-checkbox form-icon">
                <input type="checkbox"> Remember me
            </label>
        </div>
        <div class="col-xs-4">
            <div class="form-group text-right">
                <button class="btn btn-success text-uppercase" type="submit">Sign In</button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>