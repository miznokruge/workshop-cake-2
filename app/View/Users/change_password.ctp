<div class="row">
    <div class="col-lg-12 col-xs-12">
        <div class="users form">
            <?php
            echo $this->Form->create('User', array(
                'inputDefaults' => array(
                    'div' => 'form-group col-lg-12 col-xs-12',
                    'wrapInput' => false,
                    'class' => 'form-control'
                ),
                'class' => ''
            ));
            ?>
            <fieldset>
                <?php
                if ($id != '') {
                    echo $this->Form->input('id', array('type' => 'hidden', 'value' => $id));
                }
                ?>
                <?php echo $this->Form->input('password', array('type' => 'text', 'label' => array('text' => 'Enter your new password'))); ?>
                <?php echo $this->Form->input('password2', array('type' => 'text', 'label' => array('text' => 'Confirm your new password'))); ?>
            </fieldset>
            <?php echo $this->Form->end(__('Submit')); ?>
        </div>
    </div>
</div>